#ifndef muons_h
#define muons_h

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

class muons : public TSelector {
  public:
    TTreeReader fReader;
    TTree *fChain = 0;

    TTreeReaderValue<Int_t> run = {fReader, "run"};
    TTreeReaderValue<ULong64_t> event = {fReader, "event"};
    TTreeReaderValue<Bool_t> HLT_IsoMu24 = {fReader, "HLT_IsoMu24"};
    TTreeReaderValue<Int_t> PV_npvs = {fReader, "PV_npvs"};

    TH1F *hist_pileup;
    ofstream eventfile;

    muons(TTree * /*tree*/ = 0) {}
    virtual ~muons() {}
    virtual Int_t Version() const { return 2; }
    virtual void Begin(TTree *tree);
    virtual void SlaveBegin(TTree *tree);
    virtual void Init(TTree *tree);
    virtual Bool_t Notify();
    virtual Bool_t Process(Long64_t entry);
    virtual Int_t GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
    virtual void SetOption(const char *option) { fOption = option; }
    virtual void SetObject(TObject *obj) { fObject = obj; }
    virtual void SetInputList(TList *input) { fInput = input; }
    virtual TList *GetOutputList() const { return fOutput; }
    virtual void SlaveTerminate();
    virtual void Terminate();

    ClassDef(muons, 0);
};

#endif

#ifdef muons_cxx
void muons::Init(TTree *tree) {
    fReader.SetTree(tree);
}

Bool_t muons::Notify() {
    return kTRUE;
}

#endif
