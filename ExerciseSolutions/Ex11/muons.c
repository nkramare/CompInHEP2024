#define muons_cxx
#include "muons.h"
#include <TH2.h>
#include <TStyle.h>

void muons::Begin(TTree * /*tree*/) {

    TString option = GetOption();
    hist_pileup = new TH1F("pileup", "pileup for events passing HLT_IsoMu24", 50, 0, 70);
    eventfile.open("passed_events.txt");
}

void muons::SlaveBegin(TTree * /*tree*/) {
    TString option = GetOption();
}

Bool_t muons::Process(Long64_t entry) {
    fReader.SetLocalEntry(entry);

    if (*HLT_IsoMu24.Get()) {
        hist_pileup->Fill(*PV_npvs.Get());
        eventfile << *event.Get() << endl;
    }
    return kTRUE;
}

void muons::SlaveTerminate() {
}

void muons::Terminate() {

    eventfile.close();

    TCanvas *canvas = new TCanvas("canvas", "pileup distribution", 800, 600);
    hist_pileup->Draw();
        
    hist_pileup->SetLineColor(kBlack);
    hist_pileup->SetLineWidth(2);
    hist_pileup->SetFillColor(kYellow);
    
    canvas->Print("pileup_dist.pdf");
    delete canvas;

}
