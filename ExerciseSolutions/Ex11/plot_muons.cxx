void plot_muons() {
    
    gStyle->SetOptFit(11);

    TFile *file = TFile::Open("DYJetsToLL.root");
    TTree *tree = (TTree*)file->Get("Events");
    
    TCanvas *canvas = new TCanvas("canvas", "transverse momentum", 800, 600);
    tree->Draw("PV_npvs", "", "");

    TH1 *hist = (TH1*)gPad->GetPrimitive("htemp");

    hist->SetName("PV_npvs");
    hist->SetTitle("number of primary vertices");

    hist->GetXaxis()->SetTitle("PV_npvs");

    hist->GetXaxis()->SetRangeUser(0, 70);

    hist->SetLineColor(kBlack);
    hist->SetLineWidth(2);
    hist->SetFillColor(kYellow);

    canvas->Update();
    canvas->Print("PV_npvs.pdf");
    delete canvas;

    return;

    bool evt, trig;

    int num = 0;
    for (int i = 0; i < tree->GetEntries(); i++) {
        tree->GetEntry(i);

        evt = tree->GetLeaf("event")->GetValue();
        trig = tree->GetLeaf("HLT_IsoMu24")->GetValue();
    }

    //cout << "accepted " << accepted << " / " << num << " events: " << (double)accepted / (double)num * 100 << " %" << endl;
}

