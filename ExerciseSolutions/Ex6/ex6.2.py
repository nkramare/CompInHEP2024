import ROOT

def ex51(N=1000):
    file = ROOT.TFile("ex5.root", "RECREATE")
    tree = ROOT.TTree("random_tree", "tree with random numbers")
    randomNumber = ROOT.std.vector('float')()
    tree.Branch("randomNumber", randomNumber)

    #filling the tree with normally distributed random numbers
    for i in range(N):
        randomNumber.clear()
        randomNumber.push_back(ROOT.gRandom.Gaus(0, 1))
        tree.Fill()

    file.Write()
    file.Close()

def ex52():
    file = ROOT.TFile("ex5.root", "READ")
    tree = file.Get("random_tree")
    histogram = ROOT.TH1F("histogram", "random numbers", 100, -10, 10)
    tree.Draw("randomNumber>>histogram")

    #creating a canvas
    canvas = ROOT.TCanvas("canvas", "random number histogram", 800, 600)
    canvas.SetFillColor(ROOT.kWhite)

    #line and fill properties
    histogram.SetLineColor(ROOT.kBlack)
    histogram.SetLineWidth(2)
    histogram.SetFillColor(ROOT.kYellow)
    histogram.Draw()
    histogram.GetXaxis().SetTitle("Distribution")
    histogram.GetYaxis().SetTitle("Entries")
    histogram.Fit("gaus")

    #updating the canvas
    canvas.Update()
    canvas.SaveAs("random_number_histogram_py.png")

    file.Close()

def ex5():
    ex51()
    ex52()

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True) #suppress GUI
    ex5()
