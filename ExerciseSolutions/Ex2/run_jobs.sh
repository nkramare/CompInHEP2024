#!/bin/bash

# Define number of jobs
NUM_JOBS=10

# Run jobs in parallel
for ((i = 1; i <= NUM_JOBS; i++)); do
    while true; do
        read -p "Enter a number for job $i: " input_number
        if [[ $input_number =~ ^[0-9]+$ ]]; then
            break
        else
            echo "Please write the number"
        fi
    done

    output_file="output_$i.txt"
    echo "Hello world $input_number" > "$output_file"
done

echo "All jobs completed."
