#include <iostream>

int main() {
    int number;

    std::cout << "Enter a number: ";
    std::cin >> number;
    std::cout << "Hello world " << number << std::endl;

    return 0;
}
