import os
import re

def extract_number_from_log(log_file_path):
    if not os.path.exists(log_file_path):
        print("Error: Log file does not exist.")
        return None

    try:
        with open(log_file_path, 'r') as file:
            lines = file.readlines()

            #Search for the line containing 'totrecorded'
            for index, line in enumerate(lines):
                if 'totrecorded' in line:
                    #Find the column number of 'totrecorded'
                    totr_column = line.index('totrecorded')

                    # Extract the number from two lines below
                    number_line = lines[index + 2]
                    number = float(number_line[totr_column:totr_column + 10])  # Adjust the range based on column width
                    #Convert the number from pb^-1 to fb^-1 and round it with one decimal precision
                    number_fb = round(number / 1000.0, 1)
                    return number_fb
            print("Error: Number not found in log file.")
            return None
    except Exception as e:
        print(f"Error: {str(e)}")
        return None

log_file_path = '/Users/kramare/Documents/CompInHEP2024/ex2/2/brilcalc.log' #Specify the .log file location
number = extract_number_from_log(log_file_path)
if number is not None:
    print(f"Summary luminosity: {number} fb^-1")
