import subprocess

NUM_JOBS = 10

#Run jobs in parallel
for i in range(1, NUM_JOBS + 1):
    while True:
        input_number = input(f"Enter a number for job {i}: ")
        try:
            input_number = int(input_number)
            break #Breaks the loop
        except ValueError:
            print("Please type the number.")
            
    output_file = f"output_{i}.txt"
    cmd = ["./hello", str(input_number)]
    
    with open(output_file, "w") as f:
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, text=True) #starts the cpp program, captures the output and allowes the input
        stdout, _ = process.communicate(input=str(input_number))
        f.write(stdout)

print("All jobs completed.")
