#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include "TGraph.h"
#include "TCanvas.h"

void ex73() {
    
    std::ifstream dataFile1("br.sm2");
    std::ifstream dataFile2("feynhiggsdata.txt");

    //data from the first file
    std::vector<double> MHSM;
    std::vector<double> WIDTH;
    std::string line;
    //skip header line
    std::getline(dataFile1, line);
    while (std::getline(dataFile1, line)) {
        double mass, gg, gamgam, zgam, ww, zz, width;
        std::istringstream iss(line);
        if (iss >> mass >> gg >> gamgam >> zgam >> ww >> zz >> width) {
            MHSM.push_back(mass);
            WIDTH.push_back(width);
        }
    }
    dataFile1.close();

    //sort mass data
    std::vector<std::pair<double, double>> sortedData;
    sortedData.reserve(MHSM.size());
    for (size_t i = 0; i < MHSM.size(); ++i) {
        sortedData.push_back(std::make_pair(MHSM[i], WIDTH[i]));
    }
    std::sort(sortedData.begin(), sortedData.end(),
              [](const std::pair<double, double>& a, const std::pair<double, double>& b) {
                  return a.first < b.first;
              });
    for (size_t i = 0; i < sortedData.size(); ++i) {
        MHSM[i] = sortedData[i].first;
        WIDTH[i] = sortedData[i].second;
    }

    //data from the second file
    std::vector<double> feynhiggsMass;
    std::vector<double> feynhiggsWidth;
    while (std::getline(dataFile2, line)) {
        double mass, width;
        std::istringstream iss(line);
        if (iss >> mass >> width) {
            feynhiggsMass.push_back(mass);
            feynhiggsWidth.push_back(width);
        }
    }
    dataFile2.close();

    //plot
    TCanvas *canvas = new TCanvas("canvas", "canvas", 800, 1200);
    canvas->Divide(1, 2);

    //canvas 1
    canvas->cd(1);
    TGraph *graph1 = new TGraph(MHSM.size(), MHSM.data(), WIDTH.data());
    TGraph *graph2 = new TGraph(feynhiggsMass.size(), feynhiggsMass.data(), feynhiggsWidth.data());
    
    graph1->SetTitle("Higgs Width vs. Mass");
    graph1->GetXaxis()->SetTitle("Mass (GeV)");
    graph1->GetYaxis()->SetTitle("Width (GeV)");
    graph1->SetMarkerColor(kMagenta);
    graph1->SetMarkerStyle(20);
    graph1->SetMarkerSize(0.3 * graph1->GetMarkerSize());
    
    graph2->SetMarkerColor(kBlue);
    graph2->SetMarkerStyle(20);
    graph2->SetMarkerSize(0.7 * graph2->GetMarkerSize());
    
    graph1->Draw("AP");
    graph2->Draw("P SAME");
    
    gPad->SetGrid();
    gPad->SetTicks();
    
    //canvas 2 and ratio calculations
    canvas->cd(2);
    std::vector<double> ratioMass;
    std::vector<double> ratioWidth;
    for (size_t i = 0; i < MHSM.size(); ++i) {
        for (size_t j = 0; j < feynhiggsMass.size(); ++j) {
            if (MHSM[i] == feynhiggsMass[j]) {
                ratioMass.push_back(MHSM[i]);
                ratioWidth.push_back(feynhiggsWidth[j] / WIDTH[i]);
                break;
            }
        }
    }
    TGraph *graph3 = new TGraph(ratioMass.size(), ratioMass.data(), ratioWidth.data());
    graph3->SetTitle("Ratio of FeynHiggs width to HDECAY width");
    graph3->GetXaxis()->SetTitle("Mass (GeV)");
    graph3->GetYaxis()->SetTitle("Ratio");
    graph3->SetMarkerColor(kRed);
    graph3->SetMarkerStyle(20);
    graph3->Draw("AP");
    
    gPad->SetGrid();
    gPad->SetTicks();

    canvas->Draw();
}
