#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include "TGraph.h"
#include "TCanvas.h"

void ex71() {
    
    std::ifstream dataFile("br.sm2");

    std::cout << "Data:" << std::endl;
    std::string line;
    while (std::getline(dataFile, line)) {
        std::cout << line << std::endl;
    }
    dataFile.close();
    
    dataFile.open("br.sm2");
    //skip header line
    std::getline(dataFile, line);

    std::vector<double> MHSM;
    std::vector<double> WIDTH;
    double mass, gg, gamgam, zgam, ww, zz, width;
    while (std::getline(dataFile, line)) {
        std::istringstream iss(line);
        if (iss >> mass >> gg >> gamgam >> zgam >> ww >> zz >> width) {
            MHSM.push_back(mass);
            WIDTH.push_back(width);
        }
    }
    dataFile.close();

    //sort mass data
    std::vector<std::pair<double, double>> data;
    for (size_t i = 0; i < MHSM.size(); ++i) {
        data.push_back(std::make_pair(MHSM[i], WIDTH[i]));
    }
    std::sort(data.begin(), data.end());

    //print sorted data
    std::cout << "Sorted data:" << std::endl;
    for (const auto& d : data) {
        std::cout << std::fixed << std::setprecision(4) << d.first << " " << d.second << std::endl;
    }

    //convert sorted data to arrays
    int nPoints = data.size();
    std::cout << "Number of data points: " << nPoints << std::endl;
    double *massArray = new double[nPoints];
    double *widthArray = new double[nPoints];
    for (int i = 0; i < nPoints; ++i) {
        massArray[i] = data[i].first;
        widthArray[i] = data[i].second;
    }

    //plot
    TGraph *graph = new TGraph(nPoints, massArray, widthArray);
    graph->SetTitle("SM Higgs width vs. Higgs mass");
    graph->GetXaxis()->SetTitle("Higgs mass (GeV)");
    graph->GetYaxis()->SetTitle("Higgs width (GeV)");

    TCanvas *canvas = new TCanvas("canvas", "canvas", 800, 600);
    graph->Draw("ALP");
    canvas->SetGrid();
    canvas->SetTicks();
    canvas->Draw();

    delete[] massArray;
    delete[] widthArray;
}
