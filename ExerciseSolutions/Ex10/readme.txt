In example B4 the detector structure is the following:


        |<----layer 0---------->|<----layer 1---------->|<----layer 2---------->|
        |                       |                       |                       |
        ==========================================================================
        ||              |       ||              |       ||              |       ||
        ||              |       ||              |       ||              |       ||
 beam   ||   absorber   |  gap  ||   absorber   |  gap  ||   absorber   |  gap  ||
======> ||              |       ||              |       ||              |       ||
        ||              |       ||              |       ||              |       ||
        ==========================================================================

source: https://gitlab.cern.ch/geant4/geant4/-/tree/master/examples/basic/B4

The default absorber material in Pb, gap is liquid Ar. Detector contains 10 layers 10.*mm thick absorber, 5.*mm thick gap.
Detector type: Calorimeter, XY sizes: 12.*cm, Z(thickness) = 1.2 * nofLayers * layerThickness = 1.2 * 10 * 10.*mm + 5.*mm.
