import math

class Track:
    def __init__(self, px, py, pz, energy):
        self._px = px
        self._py = py
        self._pz = pz
        self._energy = energy
    
    def get_momentum(self):
        return self._px, self._py, self._pz, self._energy
    
    def get_transverse_momentum(self):
        pt = math.sqrt(self._px**2 + self._py**2)
        return pt
    
    def get_pseudorapidity(self):
        theta = math.atan2(math.sqrt(self._px**2 + self._py**2), self._pz)
        eta = -math.log(math.tan(theta/2))
        return eta

class SimulatedParticle(Track):
    def __init__(self, px, py, pz, energy, particle_id, parent_particle_id):
        super().__init__(px, py, pz, energy)
        self._particle_id = particle_id
        self._parent_particle_id = parent_particle_id
    
    def get_particle_info(self):
        return self.get_momentum(), self._particle_id, self._parent_particle_id
        
#test program
if __name__ == "__main__":
    #test Track class
    track = Track(px=10, py=10, pz=20, energy=30)
    print("track momentum:", track.get_momentum())
    print("transverse momentum:", track.get_transverse_momentum())
    print("pseudorapidity:", track.get_pseudorapidity())

    #test SimulatedParticle class
    simulated_particle = SimulatedParticle(px=15, py=10, pz=20, energy=30, particle_id=1, parent_particle_id=2)
    print("\nsimulated particle info:", simulated_particle.get_particle_info())
    print("transverse momentum:", simulated_particle.get_transverse_momentum())
    print("pseudorapidity:", simulated_particle.get_pseudorapidity())
