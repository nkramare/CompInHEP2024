import hist
import dask
import awkward as ak
import hist.dask as hda
import dask_awkward as dak

from coffea import nanoevents
from coffea.nanoevents import NanoEventsFactory, BaseSchema
from coffea import processor
from coffea.nanoevents.methods import candidate, vector

import matplotlib.pyplot as plt
import boost_histogram as bh

class Analysis(processor.ProcessorABC):
    def __init__(self):
        self.histograms = {}
        self.histograms["mass"] = hda.Hist.new.Reg(200, 0, 200, name="x", label="mass").Double()
        self.histograms["pileup"] = hda.Hist.new.Reg(51, 0, 50, name="x", label="pileup").Double()

    def process(self, events):

        muons = ak.zip(
            {
                "pt": events.Muon_pt,
                "eta": events.Muon_eta,
                "phi": events.Muon_phi,
                "mass": events.Muon_mass,
                "charge": events.Muon_charge,
                "pileup": events.PV_npvs,
                "HLT_IsoMu24": events.HLT_IsoMu24,
            },
            with_name="PtEtaPhiMCandidate",
            behavior=candidate.behavior,
        )

        cut = (ak.num(muons) == 2) & (ak.sum(muons.charge, axis=1) == 0)
        #1st + 2nd muon
        dimuon = muons[cut][:, 0] + muons[cut][:, 1]
        self.histograms["mass"].fill(x=dimuon.mass)

        isomu = muons[muons.HLT_IsoMu24]
        print(isomu.type)
        self.histograms["pileup"].fill(x=ak.ravel(isomu.pileup))

        out = {}
        out.update(self.histograms)
        return out

    def postprocess(self, accumulator):
        pass


def main():
    filename = 'DYJetsToLL.root'
    events = NanoEventsFactory.from_root(
        {filename: "Events"},
        metadata={"dataset": "DoubleMuon"},
        schemaclass=BaseSchema,
    ).events()
    p = Analysis()
    out = p.process(events)
    (result,) = dask.compute(out)
        
    h = bh.Histogram(result['pileup'])
    plt.bar(h.axes[0].centers, h.values(), width=h.axes[0].widths)
    plt.xlabel("µ")
    plt.title("pileup")
    plt.savefig("coffea_pileup.pdf")
    plt.show()


if __name__ == "__main__":
    main()
