import datetime

import ROOT

ROOT.ROOT.EnableImplicitMT()

def main():
    filename = 'DYJetsToLL.root'

    df = ROOT.RDataFrame("Events", filename)
    df = df.Filter("nMuon == 2", "two muons")
    df = df.Filter("Muon_charge[0] + Muon_charge[1] == 0", "opposite charge")

    df_dimuon = df.Define("Dimuon_mass", "InvariantMass(Muon_pt, Muon_eta, Muon_phi, Muon_mass)")
    histo = df_dimuon.Histo1D(("h_mass", ";x-axis;y-axis", 200, 0, 200), "Dimuon_mass")


    df2 = ROOT.RDataFrame("Events", filename)
    df2 = df2.Filter("HLT_IsoMu24", "HLT_IsoMu24 flag")

    histo2 = df2.Histo1D(("pileup", ";#mu;", 50, 0, 50), "PV_npvs")

    fOUT = ROOT.TFile.Open("output_rdataframe.root","RECREATE")

    days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    now = datetime.datetime.now()
    m = "produced: %s %s"%(days[now.weekday()],now)
    timestamp = ROOT.TNamed(m,"")
    timestamp.Write()

    histo.Write()

    c = ROOT.TCanvas("c", "c", 800, 600)
    histo2.Draw()
    histo2.SetLineColor(ROOT.kBlack)
    histo2.SetLineWidth(2)
    histo2.SetFillColor(ROOT.kYellow)
    c.SaveAs("rdataframe_pileup.pdf")
    histo2.Write()
    c.Close()

    fOUT.Close()

if __name__ == "__main__":
    main()
