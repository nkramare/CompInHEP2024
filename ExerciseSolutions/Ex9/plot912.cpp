#include <iostream>
#include <fstream>
#include <regex>
#include <cmath>
#include "TH1F.h"
#include "TCanvas.h"

int main() {
    std::ifstream inputFile("main912.out");
    if (!inputFile.is_open()) {
        std::cerr << "error: unable to open input file." << std::endl;
        return 1;
    }

    std::regex momentum_re("muon pt,eta,phi (\\S+) (\\S+) (\\S+)");
    std::regex nev_re("number of events (\\d+)");

    const int nbins = 50;
    TH1F h_pt("h_pt", "", nbins, 0, 10);
    TH1F h_eta("h_eta", "", nbins, -5, 5);

    int genEvents = 0;
    int allMuonEvents = 0;
    int passedEvents = 0;

    std::string line;
    while (std::getline(inputFile, line)) {
        std::smatch nev_match;
        if (std::regex_search(line, nev_match, nev_re)) {
            genEvents = std::stoi(nev_match[1]);
        }

        std::smatch match;
        if (std::regex_search(line, match, momentum_re)) {
            float pt = std::stof(match[1]);
            float eta = std::stof(match[2]);

            h_pt.Fill(pt);
            h_eta.Fill(eta);

            allMuonEvents++;
            if (pt > 5 && std::fabs(eta) < 2.5) {
                passedEvents++;
            }
        }
    }

    TCanvas canvas("canvas", "", 700, 500);
    canvas.SetFillColor(0);
    canvas.Divide(2, 1);

    canvas.cd(1);
    h_pt.SetStats(0);
    h_pt.SetFillColor(5);
    h_pt.GetXaxis()->SetTitle("muon p_{T} [GeV]");
    h_pt.Draw();

    canvas.cd(2);
    h_eta.SetStats(0);
    h_eta.SetFillColor(5);
    h_eta.GetXaxis()->SetTitle("muon #eta");
    h_eta.Draw();

    canvas.Print("muondetection.pdf");

    std::cout << "number of generated events = " << genEvents << std::endl;
    std::cout << "number of muon events = " << allMuonEvents << std::endl;
    std::cout << "number of passed events = " << passedEvents << std::endl;
    std::cout << "detection probability = passed events / number of generated events = " << static_cast<float>(passedEvents) / genEvents << std::endl;

    return 0;
}
