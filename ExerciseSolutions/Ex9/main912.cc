#include <iostream>
#include <fstream>
#include "Pythia8/Pythia.h"

using namespace Pythia8;

int main() {

  Pythia pythia;
  pythia.readString("Beams:eCM = 13600.");
  pythia.readString("SoftQCD:nonDiffractive = on");
  pythia.init();

  int NEV = 100000;
  std::cout << "number of events " << NEV << std::endl;

  std::ofstream outputFile("main912.out");
  std::streambuf *coutbuf = std::cout.rdbuf();
  std::cout.rdbuf(outputFile.rdbuf());

  for (int iEvent = 0; iEvent < NEV; ++iEvent) {
    if (!pythia.next()) continue;
    for (int i = 0; i < pythia.event.size(); ++i){
      if (abs(pythia.event[i].id()) == 13){
          std::cout << "event " << iEvent << std::endl;
          std::cout << "muon pt,eta,phi "
                  << pythia.event[i].pT() << " "
                  << pythia.event[i].eta() << " "
          << pythia.event[i].phi() << std::endl;
      }
    }
  }
    std::cout << "number of events " << NEV << std::endl;

  outputFile.close();
  std::cout.rdbuf(coutbuf);

  return 0;
}

