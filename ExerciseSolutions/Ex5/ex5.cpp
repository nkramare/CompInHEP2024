#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TF1.h>
#include <TCanvas.h>

void ex51(int N = 1000) {
    TFile file("ex5.root", "RECREATE");
    TTree tree("random_tree", "tree with random numbers");
    float randomNumber;
    tree.Branch("randomNumber", &randomNumber);

    //filling the tree with normally distributed random numbers
    for (int i = 0; i < N; ++i) {
        randomNumber = gRandom->Gaus(0, 1);
        tree.Fill();
    }
    file.Write();
    file.Close();
}

void ex52() {
    TFile file("ex5.root", "READ");
    TTree *tree = (TTree*)file.Get("random_tree");
    TH1F *histogram = new TH1F("histogram", "random numbers", 100, -10, 10);
    tree->Draw("randomNumber>>histogram");

    //creating a canvas
    TCanvas *canvas = new TCanvas("canvas", "random number histogram", 800, 600);
    canvas->SetFillColor(kWhite);
    
    //line and fill properties
    histogram->SetLineColor(kBlack);
    histogram->SetLineWidth(2);
    histogram->SetFillColor(kYellow);
    histogram->Draw();
    histogram->GetXaxis()->SetTitle("Distribution");
    histogram->GetYaxis()->SetTitle("Entries");
    histogram->Fit("gaus");

    //updating the canvas
    canvas->Update();
    canvas->SaveAs("random_number_histogram.png");

    file.Close();
}

void ex5() {
    ex51();
    ex52();
}
