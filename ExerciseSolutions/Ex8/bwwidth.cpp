#include <iostream>
#include <fstream>
#include "TH1.h"
#include "TCanvas.h"
#include "TF1.h"

using namespace std;

//Breit-Wigner function
Double_t breit_wigner(Double_t *x, Double_t *par) {
    //par[0]: normalization constant
    //par[1]: resonance mass
    //par[2]: resonance width
    return par[0] * par[2] / (2 * TMath::Pi() * ((x[0] - par[1]) * (x[0] - par[1]) + par[2] * par[2] / 4));
}

void bwwidth() {
    
    TH1D* histo = new TH1D("histo", "", 100, 124.95, 125.05);
    ifstream inFile("mass.out");
    if (!inFile.is_open()) {
        cout << "Cannot open file mass.out" << endl;
        return;
    }

    double mass;
    while (inFile >> mass) {
        histo->Fill(mass); //fill the histogram
    }
    inFile.close();

    TCanvas* canvas = new TCanvas("canvas", "", 700, 400);
    canvas->SetFillColor(0);

    histo->SetStats(0);
    histo->SetFillColor(5);
    histo->GetXaxis()->SetTitle("m_{H} (GeV)");
    histo->Draw();

    //set fitting parameters
    const double fitMin = 124.9;
    const double fitMax = 125.1;
    const int nPar = 3;
    TF1* theFit = new TF1("theFit", breit_wigner, fitMin, fitMax, nPar);
    theFit->SetParameter(1, 125);
    theFit->SetParameter(0, 0.01);

    histo->Fit("theFit", "R");
    canvas->Print("bwwidth.pdf");
    cout << "Width from the fit = " << theFit->GetParameter(2) << endl;
}
