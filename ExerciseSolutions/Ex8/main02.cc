// main02.cc is a part of the PYTHIA event generator.
// Copyright (C) 2024 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; z production; tevatron

// This is a simple test program. It fits on one slide in a talk.
// It studies the pT_Z spectrum at the Tevatron.
#include <ctime>
#include <fstream>
using namespace std;
#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main() {
  // Generator. Process selection. Tevatron initialization. Histogram.
  Pythia pythia;
  //pythia.readString("Beams:idB = -2212");
  pythia.readString("Beams:eCM = 13000");
  //pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  pythia.readString("HiggsSM:all = on");
  pythia.readString("25:m0 = 125");
  pythia.readString("PDF:pSet = 8");
  //pythia.readString("PhaseSpace:mHatMin = 80.");
  //pythia.readString("PhaseSpace:mHatMax = 120.");
  pythia.init();
  //Hist pTZ("dN/dpTZ", 100, 0., 100.);
  ofstream fOUT;
  fOUT.open ("mass.out");
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < 1000; ++iEvent) {
    if (!pythia.next()) continue;
    // Loop over particles in event. Find last Z0 copy. Fill its pT.
    int iZ = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].id() == 25) iZ = i;
    double mass = pythia.event[iZ].m();
    cout << "mass = " << std::setprecision(9) << mass << endl;
    fOUT << std::setprecision(9) << mass << endl;
  // End of event loop. Statistics. Histogram. Done.
  }
  fOUT.close();
  pythia.stat();

  return 0;
}
