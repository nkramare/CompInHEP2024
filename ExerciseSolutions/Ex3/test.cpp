#include <iostream>
#include "Measurement2D.h"

int main() {
    //Measurement with x, y and error matrix
    Measurement2D measurement(5.7, 2.3, 0.2, 0.7, 0.1);

    std::cout << "measurement: (" << measurement.getX() << ", " << measurement.getY() << ")" << std::endl;
    std::cout << "error matrix: " << std::endl;
    std::cout << "sigma X: " << measurement.getSigmaX() << std::endl;
    std::cout << "sigma Y: " << measurement.getSigmaY() << std::endl;
    std::cout << "covariance XY: " << measurement.getCovarianceXY() << std::endl;
    std::cout << "distance: " << measurement.getDistance() << std::endl;
    std::cout << "error: " << measurement.getDistanceError() << std::endl;
    std::cout << "significance: " << measurement.getSignificance() << std::endl;

    return 0;
}
