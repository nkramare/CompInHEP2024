#ifndef MEASUREMENT2D_H
#define MEASUREMENT2D_H

#include <cmath>

class Measurement2D {
private:
    double x;
    double y;
    double sigma_x;
    double sigma_y;
    double covariance_xy;

public:
    //Constructor
    Measurement2D(double x_value, double y_value, double sigma_x_value, double sigma_y_value, double covariance_xy_value)
        : x(x_value), y(y_value), sigma_x(sigma_x_value), sigma_y(sigma_y_value), covariance_xy(covariance_xy_value) {}

    //x and y coordinates
    double getX() const { return x; }
    double getY() const { return y; }

    //Error matrix (variances and covariances)
    double getSigmaX() const { return sigma_x; }
    double getSigmaY() const { return sigma_y; }
    double getCovarianceXY() const { return covariance_xy; }

    //r = sqrt(x^2 + y^2)
    double getDistance() const {
        return sqrt(x * x + y * y);
    }

    //Error
    double getDistanceError() const {
        double dx = x / getDistance();
        double dy = y / getDistance();
        double error = sqrt(dx * dx * sigma_x * sigma_x + dy * dy * sigma_y * sigma_y + 2 * dx * dy * covariance_xy);
        return error;
    }

    //Significance
    double getSignificance() const {
        double significance = getDistance() / getDistanceError();
        return significance;
    }
};

#endif
