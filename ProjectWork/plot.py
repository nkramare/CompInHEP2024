import ROOT
import pandas as pd

def plot(filename:str):
    f = ROOT.TFile.Open(filename)
    tree = f.Get('Events')

    c = ROOT.TCanvas('canvas', 'muons or sth', 20, 20, 1000, 1000)
    c.Divide(2, 2)

    c.cd(1)
    tree.Draw("Muon_eta")

    c.cd(2)
    tree.Draw("Muon_pt")

    c.cd(3)
    tree.Draw("Muon_theta")

    c.cd(4)
    tree.Draw("Muon_phi")

    c.SaveAs(filename.replace('.root', '.pdf'))


def main():
    plot('mumu.root')
    plot('dy.root')
    plot('ttbar.root')


if __name__ == '__main__':
    main()

