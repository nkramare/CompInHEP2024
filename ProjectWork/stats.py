from glob import glob

L_INT = 300  # fb^-1

files = ['mumu*.output', 'dy*.output', 'ttbar*.output']


def get_info(filename):
    with open(filename, encoding='utf-8') as f:
        for line in f:
            if line.startswith(' | sum '):
                info = line.split()
                # 3 4 5 7 8
                output = {
                    'file': filename,
                    'tried': info[3],
                    'selected': info[4],
                    'accepted': info[5],
                    'efficiency': round(float(info[5]) / float(info[3]), 4),
                    'inv_eff': round(float(info[3]) / float(info[5]), 4),
                    'xsec (mb)': info[7],  # mb
                    'xsec_err (mb)': info[8],  # mb
                    'events': int(float(info[7]) * L_INT * 1e12),
                    'events_err': int(float(info[8]) * L_INT * 1e12)
                }

                for k, v in output.items():
                    print(f'{k:<15}{v}')
                print()


def main():
    for file in files:
        file_list = glob(file)
        for f in file_list:
            get_info(f)


if __name__ == '__main__':
    main()

