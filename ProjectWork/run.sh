#!/bin/bash
#source /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-clang16-opt/setup.sh
make
N=100000
for i in {1..20}
do
    ./generate ttbar $N > "ttbar${i}.output" &
done

exit 0

N=100000
for i in {1..1}
do
    ./generate mumu $N > "mumu${i}.output" &
    ./generate dy $N > "dy${i}.output" &
    ./generate ttbar $N > "ttbar${i}.output" &
done

for i in {2..10}
do
    ./generate dy $N > "dy${i}.output" &
    ./generate ttbar $N > "ttbar${i}.output" &
done

wait

exit 0

for i in {4..13}
do
    ./generate ttbar $N > "ttbar${i}.output" &
done

wait

make clean

exit 0

python3 stats.py
python3 analysis.py
