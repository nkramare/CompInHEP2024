"""
https://root.cern.ch/doc/master/df104__HiggsToTwoPhotons_8py.html
"""

import ROOT
import os

ROOT.ROOT.EnableImplicitMT()

SMEAR_PT = 0.01
SMEAR_ETA = 0.002  # rad

# Calculate by hand
N_MUMU = 2225
N_DY = 8.229e6
N_TTBAR = 1.949e6

# Read these 2 from the output of this script
SIM_MUMU = 3e5
SIM_DY = 3e6
SIM_TTBAR = 3e6

EFF_MUMU = 0.2174
EFF_DY = 0.1158
EFF_TTBAR = 0.0164

df = {}
df["mumu"] = ROOT.RDataFrame("Events", "mumu*.root")
df["dy"] = ROOT.RDataFrame("Events", "dy*.root")
df["ttbar"] = ROOT.RDataFrame("Events", "ttbar*.root")
processes = list(df.keys())

# Apply scale factors and MC weight for simulated events relative to signal

# df['mumu'] = df['mumu'].Define("weight", "1");
# df['dy'] = df['dy'].Define("weight", "1");
# df['ttbar'] = df['ttbar'].Define("weight", "1");

df["mumu"] = df["mumu"].Define("weight", "1")
df["dy"] = df["dy"].Define("weight", f"{SIM_DY}/{SIM_MUMU}")
df["ttbar"] = df["ttbar"].Define("weight", f"{SIM_TTBAR}/{SIM_MUMU}")

# Smear eta and pt of the muons
# Small angle approximation: 1 % change in theta and phi is equivalent to 1 % change in eta
for p in processes:
    df[p] = df[p].Define("Muon_pt_meas", f"Muon_pt * (1 + {SMEAR_PT} * gRandom->Gaus())")\
                 .Define("Muon_eta_meas", f"Muon_eta + {SMEAR_ETA} * gRandom->Gaus()")
    # df[p] = df[p].Define("Muon_pt_meas", f"Muon_pt")\
    #             .Define("Muon_eta_meas", f"Muon_eta")

# Select the events for the analysis
for p in processes:
    # Apply preselection cut on photon trigger
    # df[p] = df[p].Filter("trigP")

    # Find two good muons with tight ID, pt > 25 GeV and not in the transition region between barrel and encap
    # df[p] = df[p].Define("goodphotons", "photon_isTightID && (photon_pt > 25000) && (abs(photon_eta) < 2.37) && ((abs(photon_eta) < 1.37) || (abs(photon_eta) > 1.52))")\
    #             .Filter("Sum(goodphotons) == 2")

    df[p] = df[p].Filter("nMuon == 2", "nMuon")

    df[p] = df[p].Define("goodpt", "Muon_pt_meas > 20")
    df[p] = df[p].Filter("Sum(goodpt) == 2", "pT")

    df[p] = df[p].Define("goodeta", "abs(Muon_eta_meas) < 2.1")
    df[p] = df[p].Filter("Sum(goodeta) == 2", "eta")

    # df[p] = df[p].Define("ptcut", "(abs(Muon_pt_meas[0] - Muon_pt_meas[1]) < 0.01) && True")\
    #             .Filter("Sum(ptcut) == 2")

    #df[p] = df[p].Define("goodcharge", "Muon_charge[0] + Muon_charge[1] == 0")
    #df[p] = df[p].Define("goodcharge", "Muon_charge == 0")
    #df[p] = df[p].Define("goodiso", "Muon_pt_meas[0] - Muon_pt_meas[1] < 0.1")

    # df[p] = df[p].Filter("abs(Muon_phi[0] - Muon_phi[1]) > 0.2")

    df[p] = df[p].Filter("Sum(Muon_charge) == 0", "charge")
    #df[p] = df[p].Filter("StdDev(Muon_pt_meas) < 0.5", "pT iso")



# Define a new column with the invariant mass and perform final event selection
hists = {}
for i, p in enumerate(processes):
    # Make four vectors and compute invariant mass
    df[p] = df[p].Define("Dimuon_mass", "InvariantMass(Muon_pt_meas, Muon_eta_meas, Muon_phi, Muon_mass)")

    df[p] = df[p].Filter("(Dimuon_mass >= 110) && (Dimuon_mass <= 150)", "mass")

    print(f"\n{p}:")

    df[p].Report().Print()

    # Book histogram of the invariant mass with this selection
    hists[p] = df[p].Histo1D(ROOT.RDF.TH1DModel(p, "Dimuon invariant mass; m_{#mu#mu} [GeV];Events", 30, 115, 150), "Dimuon_mass", "weight")

# Run the event loop

# RunGraphs allows to run the event loops of the separate RDataFrame graphs
# concurrently. This results in an improved usage of the available resources
# if each separate RDataFrame can not utilize all available resources, e.g.,
# because not enough data is available.
ROOT.RDF.RunGraphs([hists[s] for s in ["mumu", "dy", "ttbar"]])

mumu = hists["mumu"].GetValue()
dy = hists["dy"].GetValue()
ttbar = hists["ttbar"].GetValue()

# Plot
c = ROOT.TCanvas("c1", "muons or sth", 20, 20, 1000, 1000)
c.Divide(2, 2)
c.cd(1)
mumu.Draw()
c.cd(2)
dy.Draw()
c.cd(3)
ttbar.Draw()
c.SaveAs("hists.pdf")
c.Close()

# Scale simulated events with luminosity * cross-section / sum of weights and merge to signal
# TODO
# """One can scale a histogram such that the bins integral is equal to the normalization parameter via TH1::Scale(Double_t norm),
#    where norm is the desired normalization divided by the integral of the histogram."""
# ALSO SCALE WITH TRIGGER EFFICIENCY
# remember that the histos are already weighted relative to mumu
print(f'mumu: {N_MUMU / SIM_MUMU / EFF_MUMU}, DY: {N_DY / SIM_MUMU / EFF_DY}, ttbar: {N_TTBAR / SIM_MUMU / EFF_TTBAR}')

mumu.Scale(N_MUMU / SIM_MUMU / EFF_MUMU)
dy.Scale(N_DY / SIM_MUMU / EFF_DY)
ttbar.Scale(N_TTBAR / SIM_MUMU / EFF_TTBAR)

# Create canvas with pads for main plot ratio
c = ROOT.TCanvas("c", "", 700, 750)

upper_pad = ROOT.TPad("upper_pad", "", 0, 0.35, 1, 1)
lower_pad = ROOT.TPad("lower_pad", "", 0, 0, 1, 0.35)
for p in [upper_pad, lower_pad]:
    p.SetLeftMargin(0.14)
    p.SetRightMargin(0.05)
    p.SetTickx(False)
    p.SetTicky(False)
upper_pad.SetBottomMargin(0)
lower_pad.SetTopMargin(0)
lower_pad.SetBottomMargin(0.3)

upper_pad.Draw()
lower_pad.Draw()

data = mumu.Clone()
data.Add(dy)
data.Add(ttbar)

# Fit signal + background model to data
fit = ROOT.TF1("fit", "([0] + [1]*x + [2]*x^2 + [3]*x^3) + [4]*exp( -0.5*( (x-[5]) / [6] )^2 )", 115, 150)

# placeholders for now
fit.FixParameter(4, 119.1)
#fit.FixParameter(4, 20)
#fit.SetParLimits(4, 0, 1000)
fit.FixParameter(5, 125)
fit.FixParameter(6, 1.25)

fit.SetLineColor(2)
fit.SetLineStyle(1)
fit.SetLineWidth(2)
data.Fit("fit", "0", "", 115, 150)

# Draw data
upper_pad.cd()
data.SetMarkerStyle(20)
data.SetMarkerSize(1.2)
data.SetLineWidth(2)
data.SetLineColor(ROOT.kBlack)
data.SetMinimum(0)
# data.SetMaximum(8e3)
data.GetYaxis().SetLabelSize(0.045)
data.GetYaxis().SetTitleSize(0.05)
data.SetStats(0)
data.SetTitle("")
data.Draw("E")

# Draw fit
fit.Draw("SAME")

# Draw background
bkg = ROOT.TF1("bkg", "([0] + [1]*x + [2]*x^2 + [3]*x^3)", 115, 150)
for i in range(4):
    bkg.SetParameter(i, fit.GetParameter(i))
bkg.SetLineColor(4)
bkg.SetLineStyle(2)
bkg.SetLineWidth(2)
bkg.Draw("SAME")

# Signal
higgs = mumu.Clone()
higgs.Draw("HIST SAME")
higgs.SetStats(0)


print('')

# Draw ratio
lower_pad.cd()

ratiobkg = ROOT.TH1I("zero", "", 100, 115, 150)
ratiobkg.SetLineColor(4)
ratiobkg.SetLineStyle(2)
ratiobkg.SetLineWidth(2)
ratiobkg.SetMinimum(-125)
ratiobkg.SetMaximum(250)
ratiobkg.GetXaxis().SetLabelSize(0.08)
ratiobkg.GetXaxis().SetTitleSize(0.12)
ratiobkg.GetXaxis().SetTitleOffset(1.0)
ratiobkg.GetYaxis().SetLabelSize(0.08)
ratiobkg.GetYaxis().SetTitleSize(0.09)
ratiobkg.GetYaxis().SetTitle("Data - Bkg.")
ratiobkg.GetYaxis().CenterTitle()
ratiobkg.GetYaxis().SetTitleOffset(0.7)
ratiobkg.GetYaxis().SetNdivisions(503, False)
ratiobkg.GetYaxis().ChangeLabel(-1, -1, 0)
ratiobkg.GetXaxis().SetTitle("m_{#mu#mu} (GeV)")
ratiobkg.SetStats(0)
ratiobkg.Draw("AXIS")

ratiosig = ROOT.TH1F("ratiosig", "ratiosig", 5500, 115, 150)
ratiosig.Eval(fit)
ratiosig.SetLineColor(2)
ratiosig.SetLineStyle(1)
ratiosig.SetLineWidth(2)
ratiosig.SetStats(0)
ratiosig.Add(bkg, -1)
ratiosig.Draw("SAME")

ratiodata = data.Clone()
ratiodata.Add(bkg, -1)
for i in range(1, data.GetNbinsX()):
    ratiodata.SetBinError(i, data.GetBinError(i))
ratiodata.SetStats(0)
ratiodata.Draw("E SAME")

# Add legend
upper_pad.cd()
legend = ROOT.TLegend(0.55, 0.55, 0.89, 0.85)
legend.SetTextFont(42)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
legend.SetTextSize(0.05)
legend.SetTextAlign(32)
legend.AddEntry(data, "Data", "lep")
legend.AddEntry(bkg, "Background", "l")
legend.AddEntry(fit, "Signal + Bkg.", "l")
legend.AddEntry(mumu, "Signal", "l")
legend.Draw()

# Add label
text = ROOT.TLatex()
text.SetNDC()
text.SetTextFont(72)
text.SetTextSize(0.05)
text.DrawLatex(0.18, 0.84, "HIP")
text.SetTextFont(42)
text.DrawLatex(0.18 + 0.13, 0.84, "Simulation")
text.SetTextSize(0.04)
text.DrawLatex(0.18, 0.78, "#sqrt{s} = 13.6 TeV, 300 fb^{-1}")

# Save the plot
c.SaveAs("dimuon.pdf")
