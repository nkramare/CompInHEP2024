/*
Simulate Higgs decay to muons
*/
#include <unistd.h>
#include "Pythia8/Pythia.h"
#include <TFile.h>
#include <TTree.h>
#include "ROOT/RVec.hxx"

using namespace Pythia8;

int main(int argc, char *argv[]) {
    if (argc < 3) {
        cout << "Usage: " << argv[0] << " <process: mumu|dy|ttbar> <number of events>" << endl;
        return 1;
    }

    std::string process(argv[1]);
    int n_event = atoi(argv[2]);

    Pythia p;

    // Run 3 LHC
    p.readString("Beams:eCM = 13600.");

    // Let's make the pT and mass cuts here already to improve performance
    p.readString("PhaseSpace:pTHatMin = 20.");
    p.readString("PhaseSpace:mHatMin = 110");

    if (process == "mumu") {
        // Small optimization
        //p.readString("PhaseSpace:mHatMax = 150");

        // Higgs mass
        p.readString("25:m0 = 125");

        // Higgs production
        p.readString("HiggsSM:all = on");

        // Higgs decay: all off
        p.readString("25:onMode = off");

        // Higgs decay: enable dimuon decay
        p.readString("25:onIfMatch = -13 13");
    } else if (process == "dy") {
        // Small optimization
        //p.readString("PhaseSpace:mHatMax = 150");

        p.readString("WeakSingleBoson:ffbar2ffbar(s:gmZ) = on");

        // Z decay: all off
        p.readString("23:onMode = off");
        p.readString("-23:onMode = off");

        // Only Z decays to muons are interesting
        p.readString("23:onIfMatch = -13 13");
        p.readString("-23:onIfMatch = -13 13");
    } else if (process == "ttbar") {
        // ttbar https://pythia.org//latest-manual/TopProcesses.html
        p.readString("Top:gg2ttbar = on");
        p.readString("Top:qqbar2ttbar = on");
        p.readString("Top:ffbar2ttbar(s:gmZ) = on");
        p.readString("Top:gmgm2ttbar = on");
        p.readString("Top:ggm2ttbar = on");

        // W decay: all off
        p.readString("24:onMode = off");
        p.readString("-24:onMode = off");

        // W decay: enable decay to muon
        // https://physics.stackexchange.com/questions/517721/what-are-the-allowed-w-bosons-in-muon-decay
        p.readString("24:onIfMatch = 13 14");
        p.readString("-24:onIfMatch = -13 -14");
    }else{
        cout << "Unknown process: " << process << endl;
        return 1;
    }

    p.init();

    TFile file(process.append(std::to_string(getpid()).c_str()).append(".root").c_str(), "RECREATE");
    TTree tree("Events", "Kinematic parameters of muons");

    int iEvent, nMuon;
    ROOT::RVec<double> pt, eta, phi, m, theta, charge;

    tree.Branch("event", &iEvent);
    tree.Branch("Muon_pt", &pt);
    tree.Branch("Muon_eta", &eta);
    tree.Branch("Muon_phi", &phi);
    tree.Branch("Muon_mass", &m);
    tree.Branch("Muon_theta", &theta);
    tree.Branch("Muon_charge", &charge);
    tree.Branch("nMuon", &nMuon);

    Hist hist_eta("Rapidity", 200, -10, 10);
    Hist hist_pt("Transverse momentum", 100, 0, 200);

    // Begin event loop. Generate event. Skip if error. List first one.
    for (iEvent = 0; iEvent < n_event; ++iEvent) {
        if (!p.next())
            continue;

        // Number of muons per event
        nMuon = 0;
        for (int i = 0; i < p.event.size(); ++i) {
            Particle par = p.event[i];
            if (par.id() == 13 || par.id() == -13) {
                nMuon++;
            }
        }

        pt = eta = phi = m = theta = charge = {};
        // Find number of all final charged particles and fill histograms etc.
        for (int i = 0; i < p.event.size(); ++i) {
            // https://pythia.org/latest-manual/EventRecord.html
            Particle par = p.event[i];
            if (par.id() == 13 || par.id() == -13) {
                //cout << "evt " << i << ", pT " << par.pT() <<  ", eta " << par.y() << ", phi " << par.phi() << ", m " << par.m() << ", charge " << par.chargeType() << endl;
                hist_eta.fill(par.y());  // Note: PYTHIA uses eta for pseudo-rapidity and y for rapidity, we use ROOT's convention
                hist_pt.fill(par.pT());

                pt.emplace_back(par.pT());
                eta.emplace_back(par.y());
                phi.emplace_back(par.phi());
                m.emplace_back(par.m());
                theta.emplace_back(par.theta());
                charge.emplace_back(par.chargeType());
            }
        }
        tree.Fill();
    }

    tree.Write();
    file.Close();

    p.stat();
    cout << hist_eta << endl << endl << hist_pt;

    return 0;
}
